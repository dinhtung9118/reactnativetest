import favourites from '../favouritesReducer';

describe('favourites reducer case', () => {
  it('set default favourites', () => {
    expect(favourites(undefined, {type: 'unexpected'})).toEqual({
      users: [],
    });
  });
  it('set favourites by action type SET_FAVOURITES ', () => {
    const initData = {
      users: [],
    };
    expect(favourites(initData, {type: 'SET_FAVOURITES', payload: []})).toEqual(
      {
        users: [],
      },
    );
  });
});

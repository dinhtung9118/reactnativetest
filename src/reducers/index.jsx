import {combineReducers} from 'redux';
import favourites from './favouritesReducer';

const rootReducer = combineReducers({
  favourites: favourites,
});

export default rootReducer;

import {SET_FAVOURITES} from '../actions/types';

const initialState = {
  users: [],
};

const favourites = (state = initialState, action) => {
  switch (action.type) {
    case SET_FAVOURITES: {
      return {
        ...state,
        users: action.payload,
      };
    }
    default: {
      return state;
    }
  }
};
export default favourites;

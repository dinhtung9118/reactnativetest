import {SET_FAVOURITES} from './types';
export const reduxSetFavourites = data => ({type: SET_FAVOURITES, data: data});

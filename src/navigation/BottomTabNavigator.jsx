import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import {createBottomTabNavigator, BottomTabBar} from 'react-navigation-tabs';

import React from 'react';
import Home from '../screens/Home';
import Favourites from '../screens/Favourites';
import Icon from 'react-native-vector-icons/FontAwesome5';

const BottomTabNavigator = createBottomTabNavigator(
  {
    Home: {
      screen: Home,
    },
    Favourites: {
      screen: Favourites,
    },
  },
  {
    defaultNavigationOptions: ({navigation}) => ({
      tabBarIcon: ({focused, horizontal, tintColor}) => {
        const {routeName} = navigation.state;
        if (routeName === 'Home') {
          return <Icon name="home" size={20} color={tintColor} />;
        } else {
          return <Icon name="heart" size={20} color={tintColor} />;
        }
      },
    }),
    tabBarOptions: {
      style: {backgroundColor: 'white'},
      activeTintColor: '#ff5959',
      inactiveTintColor: '#ccc',
    },
    initialRouteName: 'Home',
  },
);
const AppNavigator = createStackNavigator({
  MainScreen: {
    screen: BottomTabNavigator,
    navigationOptions: {
      header: null,
    },
  },
  initialRouteName: 'Home',
});
const AppNav = createAppContainer(AppNavigator);
export default AppNav;

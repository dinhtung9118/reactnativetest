export const fakeUsers = [
  {
    SSN: '646-57-5177',
    cell: '(625)-524-6040',
    dob: '161973853',
    email: 'leah.foster26@example.com',
    gender: 'female',
    location: {
      city: 'albuquerque',
      state: 'utah',
      street: '4232 saddle dr',
      zip: '41604',
    },
    md5: 'fd339f990cb5d1a6aedf1930d2565f54',
    name: {title: 'mrs', first: 'leah', last: 'foster'},
    password: 'paint',
    phone: '(234)-279-5328',
    picture: 'http://api.randomuser.me/portraits/women/19.jpg',
    registered: '974485272',
    salt: 'XZ2hHvV7',
    sha1: '9e84555c74bddcc42bbef117cdc094da527efdf9',
    sha256: 'bdbd776b5521b489e10b11506d27c7184fced304e5feb6174b296f39eba2bb5e',
    username: 'whiteladybug160',
  },
  {
    SSN: '868-44-7582',
    cell: '(938)-438-1230',
    dob: '256359262',
    email: 'herbert.miller53@example.com',
    gender: 'male',
    location: {
      city: 'eugene',
      state: 'arizona',
      street: '3354 northaven rd',
      zip: '99418',
    },
    md5: 'b58a24872e93b94f8f198c910f58043e',
    name: {
      first: 'herbert',
      last: 'miller',
      title: 'mr',
    },
    password: 'cumm',
    phone: '(140)-516-2337',
    picture: 'http://api.randomuser.me/portraits/men/91.jpg',
    registered: '1202542912',
    salt: 'dvgLacrZ',
    sha1: '675db592599e17782d0ad87ab5183285037c8ebb',
    sha256: '6d49d22e3412954ff3a952f9523eb53ddbf4555675603db3e982608b50543c05',
    username: 'tinymeercat636',
  },
];

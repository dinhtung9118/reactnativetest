import React from 'react';
import Avatar from '../Avatar';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {testSnapshots} from '../../utils/test.util';

describe('Favourites snapshot', () => {
  testSnapshots(Avatar, [
    {
      props: {
        urlAvatar: 'http://api.randomuser.me/portraits/women/19.jpg',
      },
      description: 'default render',
    },
  ]);
});

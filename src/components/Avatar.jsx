import React, {PureComponent} from 'react';
import {Image, View, StyleSheet} from 'react-native';

class Avatar extends PureComponent {
  render() {
    return (
      <View style={styles.containerAvatar}>
        <Image
          style={styles.avatar}
          source={{
            uri: this.props.urlAvatar,
          }}
        />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  containerAvatar: {
    alignItems: 'center',
  },
  avatar: {
    marginTop: -100,
    borderRadius: 150,
    borderWidth: 5,
    borderColor: '#ffffff',
    width: 150,
    height: 150,
    resizeMode: 'contain',
  },
});
export default Avatar;

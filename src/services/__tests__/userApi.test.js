import mockAxios from 'axios';

import {getUser, makeRequest} from '../userApi';

// Make sure to resolve with a promise
it('fetches data from api', async () => {
  const res = await getUser();
  expect(res.results.length > 0).toBe(true);
  expect(Object.keys(res.results[0].user).length > 0).toBe(true);
});

it('resquest error case', async () => {
  const err = await makeRequest('Get', 'api/1/', null);
  expect(err.status).toBe(404);
});

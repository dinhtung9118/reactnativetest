import React from 'react';
import Container, {Favourites} from '../Favourites.container';
import configureStore from 'redux-mock-store';
import {shallow} from 'enzyme';

import {fakeUsers} from '../../../__mocks__/dataStore';
import {testSnapshots} from '../../../utils/test.util';

const initialState = {
  favourites: {
    users: fakeUsers,
  },
};
describe('Test connected Container', () => {
  const mockStore = configureStore();
  const store = mockStore(initialState);

  test('should connect redux', () => {
    shallow(<Container store={store} />);
  });
});
describe('Favourites snapshot', () => {
  testSnapshots(Favourites, [
    {
      props: {
        favourites: fakeUsers,
      },
      description: 'default render',
    },
  ]);
});

describe('Favourites snapshot case empty', () => {
  testSnapshots(Favourites, [
    {
      props: {
        favourites: [],
      },
      description: 'default render',
    },
  ]);
});

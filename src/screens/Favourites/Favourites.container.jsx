import React, {PureComponent} from 'react';
import {View, Text, FlatList, Image, StyleSheet} from 'react-native';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';

export class Favourites extends PureComponent {
  _keyExtractor = item => item.seed;

  _renderItem = ({item}) => (
    <View style={styles.container}>
      <Image
        style={styles.avatar}
        source={{
          uri: item.picture,
        }}
        alt="Avatar"
      />
      <View style={styles.cardInfo}>
        <Text style={styles.userName}>
          {item.name.first} {item.name.last}
        </Text>
        <Text style={styles.email}>{item.email}</Text>
      </View>
    </View>
  );

  render() {
    const {favourites} = this.props;
    return (
      <View style={{flex: 1, marginTop: 50}}>
        {favourites.length > 0 ? (
          <FlatList
            initialNumToRender={favourites.length}
            data={favourites}
            keyExtractor={this._keyExtractor}
            renderItem={this._renderItem}
          />
        ) : (
          <View style={styles.containerEmpty}>
            <Text style={styles.messageEmpty}>
              You do not have any favorites list yet
            </Text>
          </View>
        )}
      </View>
    );
  }
}

const mapStateToProps = state => ({
  favourites: state.favourites.users,
});

Favourites.propTypes = {
  favourites: PropTypes.arrayOf(PropTypes.object),
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    padding: 5,
    margin: 5,
    borderBottomWidth: 1,
    borderBottomColor: '#d4d7dd',
  },
  containerEmpty: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  messageEmpty: {
    color: '#e0e0e0',
  },
  avatar: {
    borderRadius: 50,
    borderWidth: 5,
    borderColor: '#ffffff',
    width: 50,
    height: 50,
    resizeMode: 'contain',
  },
  cardInfo: {
    marginTop: 10,
  },
  userName: {
    fontSize: 16,
  },
  email: {
    color: '#c5c5c5',
  },
});
export default connect(
  mapStateToProps,
  null,
)(Favourites);

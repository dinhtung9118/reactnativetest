import React from 'react';
import Container, {Home} from '../Home.container';
import configureStore from 'redux-mock-store';
import {shallow} from 'enzyme';

import {fakeUsers} from '../../../__mocks__/dataStore';
import {stateExitFavorites, newState} from '../__mocks__/stateMock';
import service, {getUser} from '../../../services/userApi';

const initialState = {
  favourites: {
    users: fakeUsers,
  },
};
describe('Test connected Container', () => {
  const mockStore = configureStore();
  const store = mockStore(initialState);

  test('should connect redux without crash', () => {
    shallow(<Container store={store} />);
  });
});
describe('Logic function in Home screen', () => {
  const props = {
    favourites: [],
    reduxSetFavourites: jest.fn(),
  };
  const newprops = {
    favourites: fakeUsers,
    reduxSetFavourites: jest.fn(),
  };
  it('function setActiveTab case tab == "user"', () => {
    const wrapper = shallow(<Home {...props} />);
    const instance = wrapper.instance();
    instance.setActiveTab('user');
    expect(instance.state.currentView).toEqual('user');
  });

  it('function onSwipe() case swipe left', () => {
    const wrapper = shallow(<Home {...props} />);
    const instance = wrapper.instance();
    instance.getNewUser = jest.fn();
    instance.onSwipe('SWIPE_LEFT', {});
    expect(instance.state.gestureName).toEqual('SWIPE_LEFT');
    expect(instance.getNewUser).toBeCalled();
  });

  it('function onSwipe() case swipe right', () => {
    const wrapper = shallow(<Home {...props} />);
    const instance = wrapper.instance();
    instance.getNewUser = jest.fn();
    instance.onSwipe('SWIPE_RIGHT', {});
    expect(instance.state.gestureName).toEqual('SWIPE_RIGHT');
    expect(instance.getNewUser).toBeCalled();
  });

  it('function onSwipe() case swipe up', () => {
    const wrapper = shallow(<Home {...props} />);
    const instance = wrapper.instance();
    instance.getNewUser = jest.fn();
    instance.onSwipe('SWIPE_UP', {});
    expect(instance.state.gestureName).toEqual('SWIPE_UP');
    expect(instance.getNewUser).not.toBeCalled();
  });

  it('function onSwipe() case swipe down', () => {
    const wrapper = shallow(<Home {...props} />);
    const instance = wrapper.instance();
    instance.getNewUser = jest.fn();
    instance.onSwipe('SWIPE_DOWN', {});
    expect(instance.state.gestureName).toEqual('SWIPE_DOWN');
    expect(instance.getNewUser).not.toBeCalled();
  });

  it('function pushToFavourite() case state = null', () => {
    const wrapper = shallow(<Home {...props} />);
    const instance = wrapper.instance();
    instance.checkExistUser = jest.fn();
    instance.pushToFavourite();
    expect(instance.checkExistUser).not.toBeCalled();
    expect(props.reduxSetFavourites).not.toBeCalled();
  });

  it('function pushToFavourite() case state existed in favourites', () => {
    const wrapper = shallow(<Home {...newprops} />);
    const instance = wrapper.instance();
    instance.setState({...stateExitFavorites});
    instance.checkExistUser = jest.fn();
    instance.pushToFavourite();
    expect(instance.checkExistUser).toBeCalled();
    expect(props.reduxSetFavourites).not.toBeCalled();
  });

  it('function pushToFavourite() case user do not existed in favourites', () => {
    const wrapper = shallow(<Home {...newprops} />);
    const instance = wrapper.instance();
    instance.setState({...newState});
    instance.checkExistUser = jest.fn();
    instance.pushToFavourite();
    expect(instance.checkExistUser).toBeCalled();
    expect(newprops.reduxSetFavourites).toBeCalled();
  });

  it('function onSwipeRight()', () => {
    const wrapper = shallow(<Home {...props} />);
    const instance = wrapper.instance();
    instance.pushToFavourite = jest.fn();
    instance.onSwipeRight();
    expect(instance.pushToFavourite).toBeCalled();
  });

  it('function checkExistUser() case length of favourites = 0', () => {
    const wrapper = shallow(<Home {...props} />);
    const instance = wrapper.instance();
    expect(instance.checkExistUser()).toBe(false);
  });

  it('function checkExistUser() case user existed favourites', () => {
    const wrapper = shallow(<Home {...newprops} />);
    const instance = wrapper.instance();
    instance.setState({...stateExitFavorites});
    expect(instance.checkExistUser()).toBe(true);
  });

  it('function componentWillMount()', () => {
    const wrapper = shallow(<Home {...newprops} />);
    const instance = wrapper.instance();
    instance.getNewUser = jest.fn();
    instance.componentWillMount();
    expect(instance.getNewUser).toBeCalled();
  });
});

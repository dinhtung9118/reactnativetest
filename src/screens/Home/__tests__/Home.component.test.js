import React from 'react';
import {
  ContentTabContainer,
  HeaderContainer,
} from '../Home.component';
import {testSnapshots} from '../../../utils/test.util';
describe('ContentTabContainer snapshot', () => {
  testSnapshots(ContentTabContainer, [
    {
      props: {
        lable: 'user',
      },
      description: 'default render',
    },
  ]);
});

describe('HeaderContainer snapshot', () => {
  testSnapshots(HeaderContainer, [
    {
      props: {
        children: null,
      },
      description: 'default render',
    },
  ]);
});

export const stateExitFavorites = {
  user: {
    SSN: '646-57-5177',
    cell: '(625)-524-6040',
    dob: '161973853',
    email: 'leah.foster26@example.com',
    gender: 'female',
    location: {
      city: 'albuquerque',
      state: 'utah',
      street: '4232 saddle dr',
      zip: '41604',
    },
    md5: 'fd339f990cb5d1a6aedf1930d2565f54',
    name: {title: 'mrs', first: 'leah', last: 'foster'},
    password: 'paint',
    phone: '(234)-279-5328',
    picture: 'http://api.randomuser.me/portraits/women/19.jpg',
    registered: '974485272',
    salt: 'XZ2hHvV7',
    sha1: '9e84555c74bddcc42bbef117cdc094da527efdf9',
    sha256: 'bdbd776b5521b489e10b11506d27c7184fced304e5feb6174b296f39eba2bb5e',
    username: 'whiteladybug160',
  },
  currentView: 'address',
  listTab: [
    {
      name: 'user',
      type: 'profile',
    },
    {
      name: 'list-alt',
      type: 'otherInfor',
    },
    {
      name: 'map-marked-alt',
      type: 'address',
    },
    {
      name: 'phone',
      type: 'contact',
    },
    {
      name: 'lock',
      type: 'private',
    },
  ],
};

export const newState = {
  user: {
    SSN: '584-12-3796',
    cell: '(695)-162-3730',
    dob: '313056913',
    email: 'connie.jensen10@example.com',
    gender: 'female',
    location: {
      city: 'shelby',
      state: 'indiana',
      street: '8300 sunset blvd',
      zip: '65501',
    },
    md5: '562358b9b27769b7170ead8c812451a9',
    name: {
      first: 'connie',
      last: 'jensen',
      title: 'ms',
    },
    password: '1968',
    phone: '(356)-739-8499',
    picture: 'http://api.randomuser.me/portraits/women/93.jpg',
    registered: '1386479992',
    salt: 'qZER66wL',
    sha1: '70d25136cbafd89558ffa8d6ee0a8be2b54d6893',
    sha256: '8e09506b95dc915383b67af091ab05fbe1b844b204836ca36fa8792cbad2b428',
    username: 'yellowdog230',
  },
  currentView: 'address',
  listTab: [
    {
      name: 'user',
      type: 'profile',
    },
    {
      name: 'list-alt',
      type: 'otherInfor',
    },
    {
      name: 'map-marked-alt',
      type: 'address',
    },
    {
      name: 'phone',
      type: 'contact',
    },
    {
      name: 'lock',
      type: 'private',
    },
  ],
};

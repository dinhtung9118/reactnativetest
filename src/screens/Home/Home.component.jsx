import React from 'react';
import {View, Text, StyleSheet, Dimensions} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import PropTypes from 'prop-types';
const {height, width} = Dimensions.get('window');

export function ContentTabContainer(props) {
  return (
    <View style={styles.containerTab}>
      <Text style={styles.label}>{props.label}</Text>
      <Text style={styles.content}>{props.children}</Text>
    </View>
  );
}

export function IconTabContainer(props) {
  const color = props.currentTab === props.typeTab ? '#a0c334' : '#e0e0e0';
  return (
    <React.Fragment>
      <Icon style={styles.itemIcon} name={props.name} size={30} color={color} />
    </React.Fragment>
  );
}

export function HeaderContainer() {
  return (
    <React.Fragment>
      <View style={styles.headerContainer} />
    </React.Fragment>
  );
}

IconTabContainer.propTypes = {
  currentTab: PropTypes.string,
  typeTab: PropTypes.string,
  name: PropTypes.string,
};

ContentTabContainer.propTypes = {
  label: PropTypes.string,
  children: PropTypes.any,
};

export const HeaderBackground = HeaderContainer;
export const ContentTab = ContentTabContainer;
export const IconTab = IconTabContainer;

const styles = StyleSheet.create({
  containerTab: {
    marginTop: 50,
    alignItems: 'center',
  },
  label: {
    fontSize: 16,
    color: '#979797',
    marginBottom: 10,
  },
  content: {
    fontSize: 24,
    paddingBottom: 50,
    alignItems: 'center',
  },
  itemIcon: {
    paddingLeft: 5,
    paddingRight: 5,
  },
  headerContainer: {
    width: width,
    height: height / 3,
    backgroundColor: '#252525',
  },
});

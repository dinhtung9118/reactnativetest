import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import GestureRecognizer, {swipeDirections} from 'react-native-swipe-gestures';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';

import {Container} from './Component';
import Avatar from '../../components/Avatar';
import {ContentTab, IconTab, HeaderBackground} from './Home.component';
import {getUser} from '../../services/userApi';

const {height, width} = Dimensions.get('window');

export class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: {},
      currentView: 'address',
      listTab: [
        {
          name: 'user',
          type: 'profile',
        },
        {
          name: 'list-alt',
          type: 'otherInfor',
        },
        {
          name: 'map-marked-alt',
          type: 'address',
        },
        {
          name: 'phone',
          type: 'contact',
        },
        {
          name: 'lock',
          type: 'private',
        },
      ],
    };
    this.pushToFavourite = this.pushToFavourite.bind(this);
    this.checkExistUser = this.checkExistUser.bind(this);
    this.setActiveTab = this.setActiveTab.bind(this);
  }

  componentWillMount() {
    this.getNewUser();
  }

  getNewUser() {
    getUser().then(res => {
      if (res && res.results[0]) {
        this.setState({user: res.results[0].user});
      }
    });
  }

  setActiveTab(tab) {
    this.setState({currentView: tab});
  }

  onSwipe(gestureName, gestureState) {
    const {SWIPE_LEFT, SWIPE_RIGHT} = swipeDirections;
    this.setState({gestureName: gestureName});
    switch (gestureName) {
      case SWIPE_LEFT:
      case SWIPE_RIGHT:
        this.getNewUser();
        break;
    }
  }

  onSwipeRight(gestureState) {
    this.pushToFavourite();
  }

  pushToFavourite() {
    if (this.state.user && this.state.user.email) {
      let isUserExist = this.checkExistUser();
      if (!isUserExist) {
        let newFavourites = [...this.props.favourites];
        newFavourites.push(this.state.user);
        this.props.reduxSetFavourites(newFavourites);
      }
    }
  }

  checkExistUser() {
    let exist = false;
    if (this.props.favourites.length > 0) {
      this.props.favourites.map(item => {
        if (this.state.user.email === item.email) {
          exist = true;
        }
      });
    }
    return exist;
  }

  render() {
    const {currentView, listTab} = this.state;
    const {location, phone, name, gender, username, picture} = this.state.user;
    return (
      <GestureRecognizer
        onSwipe={(direction, state) => this.onSwipe(direction, state)}
        onSwipeRight={state => this.onSwipeRight(state)}
        config={{velocityThreshold: 0.3, directionalOffsetThreshold: 80}}>
        <Container>
          <HeaderBackground />
          <View style={styles.bodyContainer}>
            <View style={styles.cardPeople}>
              <View style={styles.headerCard} />
              <React.Fragment>
                <Avatar urlAvatar={picture} />
                <React.Fragment>
                  {currentView === 'address' ? (
                    <ContentTab label="My address is">
                      {location ? location.street : ''}{' '}
                      {location ? location.city : ''}
                    </ContentTab>
                  ) : currentView === 'contact' ? (
                    <ContentTab label="My phone is">
                      {phone ? phone.toString() : ''}
                    </ContentTab>
                  ) : currentView === 'profile' ? (
                    <ContentTab label="My name is">
                      {name ? name.first : ''} {name ? name.last : ''}
                    </ContentTab>
                  ) : currentView === 'otherInfor' ? (
                    <ContentTab label="Other infor">
                      Gender: {gender}
                    </ContentTab>
                  ) : currentView === 'private' ? (
                    <ContentTab label="Private">
                      UserName: {username}
                    </ContentTab>
                  ) : (
                    <Text />
                  )}
                </React.Fragment>
                <View style={styles.listIcon}>
                  {listTab.map((item, index) => (
                    <TouchableOpacity
                      key={index}
                      onPress={() => this.setActiveTab(item.type)}>
                      <IconTab
                        name={item.name}
                        typeTab={item.type}
                        currentTab={currentView}
                      />
                    </TouchableOpacity>
                  ))}
                </View>
              </React.Fragment>
            </View>
          </View>
        </Container>
      </GestureRecognizer>
    );
  }
}

const mapStateToProps = state => ({
  favourites: state.favourites.users,
});

const mapDispatchToProps = dispatch => {
  return {
    reduxSetFavourites: payload =>
      dispatch({
        type: 'SET_FAVOURITES',
        payload: payload,
      }),
  };
};

Home.propTypes = {
  favourites: PropTypes.arrayOf(PropTypes.object),
  reduxSetFavourites: PropTypes.func,
};

const styles = StyleSheet.create({
  bodyContainer: {
    backgroundColor: '#F6F6F6',
    alignItems: 'center',
    height: height,
  },
  cardPeople: {
    marginTop: -100,
    height: 'auto',
    width: width - 40,
    padding: 4,
    alignItems: 'center',
    backgroundColor: '#ffffff',
    borderBottomColor: '#c5c5c5',
    borderBottomWidth: 1,
    borderRadius: 3,
  },
  headerCard: {
    backgroundColor: '#F6F6F6',
    width: width - 40,
    height: 150,
    borderBottomColor: '#c5c5c5',
    borderBottomWidth: 1,
  },
  containerAvatar: {
    alignItems: 'center',
  },
  listIcon: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    paddingBottom: 20,
    width: width - 40,
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Home);

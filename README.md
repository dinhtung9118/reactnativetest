#### Get started

To set up the project :
Clone the repo & `yarn`

`yarn install`

### run app
`react-native run-ios`

### run test
`yarn test -- --coverage`